Files:
ic_oldmatp_pflotran_statetrans.in (2D, 75 cells)
pflotran.noengsys_iss2_B_statetrans.in (1D, 103 cells)

Both simulations are the same in these ways:
* 2 boundary conditions - top_unsaturated and bottom_saturated
* initial conditions - use unsaturated flow condition for HGUs above the 'aquifer' and - saturated flow condition at and below the aquifer
* very simplified in terms of no gridded initial conditions, doesn't inclide engineered system, doesn't include transport, no infiltration.
 
They differ in these ways:
*  ic_oldmatp_pflotran_statetrans uses material properties that are similar to previous reference cases and pflotran.noengsys_iss2_B_statetrans uses values derived from an 'optimization' evaluation of what helped the calculations run faster and are in the ball park of published values.
* material properties - rock density, thermal conductivity, heat capacity
characteristic curves - M and alpha for liquid residual saturation and perm functions for each phase
* slightly different HGU configuration, water table. ic_oldmatp_pflotran_statetrans.in has simplified HGUs and optimized material properties and characteristic curves.
 
The apparent issue:
* use infinity_norm_convergence, phase_change_epsilon, max_cfl, and max_capillary_pressure to remove 'state transition' messages in simulation output
* even with recent fixes the calcuations don't complete efficiently enough, exceed 999,999 steps but only reach ~1e4 years by the cutoff.

NOTES for both screenshots:
left: liquid pressure at t=0, green shows location of liq_P vs time plot
right top: plot of time versus liq_P in the top part of repository horizon
bottom right: HGUs, green shows location of liq_P vs time plot, dark blue=lower basin fill, blue=upper basin fill aquifer, red = upper basin fill, peach=upper basin fill confining unit