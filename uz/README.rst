**************
UZ scenarios
**************

Scenario 1
==========
./scenarios/uz12pwr_oscillating_state - with 12-PWR heat sources, oscillating state changes occur just above the water table (cell 445). Can use this problem to play with keeping state fixed during Newton iteration, or possibly to improve gridded dataset initial conditions.

Scenario 2
==========
./scenarios/uz37pwr_dryout - with 37-PWR heat sources, dry out occurs and time steps drop to 2e-14 y. Temperature v. time curves are not smooth.

Notes
+++++++
* The additive top BC used in these input files was not used in the 1-d column runs that set up the gridded dataset for IC/BC. Columns had only infiltration. Consider making the two consistent with each other.

* Thermal conductivity of 1 W/m/K is for insitu alluvium, which would be neither dry nor wet. Necessary to revisit wet and dry values before moving on to PA calculations.

* Charles' comments about super-heated steam and lack thereof in PFLOTRAN (issue 18 37-PWR in shale) are relevant for UZ 37-PWR also.

* With addition of and updates to common/initcond/east/pflotran.in and common/initcond/west/pflotran.in and make_regionalgrad.py, you should run these and make new gridded dataset, instead of using the one that is committed in the repository.
