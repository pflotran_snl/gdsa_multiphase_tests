#!/usr/bin/env python

'''make a geothermal profile initial condition dataset for a model WITH regional head gradient '''

import h5py as h5
import numpy as N #necessary to vstack datasets

#open the files of interest
f = h5.File('./initcond.h5','w')
f2 = h5.File('./west/pflotran.h5', 'r')
f3 = h5.File('./east/pflotran.h5', 'r')

#first get the data from 1D output files
t_w = f2['Time:  1.00000E+06 y']['Temperature [C]'].value[0,0,:]
lp_w = f2['Time:  1.00000E+06 y']['Liquid_Pressure [Pa]'].value[0,0,:]
gp_w = f2['Time:  1.00000E+06 y']['Gas_Pressure [Pa]'].value[0,0,:]
gs_w = f2['Time:  1.00000E+06 y']['Gas_Saturation'].value[0,0,:]
t_e = f3['Time:  1.00000E+06 y']['Temperature [C]'].value[0,0,:]
lp_e = f3['Time:  1.00000E+06 y']['Liquid_Pressure [Pa]'].value[0,0,:]
gp_e = f3['Time:  1.00000E+06 y']['Gas_Pressure [Pa]'].value[0,0,:]
gs_e = f3['Time:  1.00000E+06 y']['Gas_Saturation'].value[0,0,:]

#create the groups
f.create_group('hydrostatic_boundary_GP')
f.create_group('hydrostatic_boundary_LP')
f.create_group('hydrostatic_boundary_T')
f.create_group('hydrostatic_boundary_GS')

#add the data
lp = N.vstack((lp_w,lp_e))
gp = N.vstack((gp_w,gp_e))
gs = N.vstack((gs_w,gs_e))
t = N.vstack((t_w,t_e))

#ensure non-zero gas pressure
gp = N.maximum(gp,lp)

#add the datasets to the groups
f['hydrostatic_boundary_LP'].create_dataset('Data',data=lp)
f['hydrostatic_boundary_GS'].create_dataset('Data',data=gs)
f['hydrostatic_boundary_GP'].create_dataset('Data',data=gp)
f['hydrostatic_boundary_T'].create_dataset('Data',data=t)

#now add the attributes to the groups
#values read in are for cell centers, cells were 1 m tall from -1 to 991, so
#assign values in output files to elevation corresponding to cell centers.
f['hydrostatic_boundary_T'].attrs.create('Dimension',['XZ'],dtype='|S2')
f['hydrostatic_boundary_T'].attrs.create('Discretization',[3930.0,10.])
f['hydrostatic_boundary_T'].attrs.create('Origin',[0.,-5.])

f['hydrostatic_boundary_LP'].attrs.create('Dimension',['XZ'],dtype='|S2')
f['hydrostatic_boundary_LP'].attrs.create('Discretization',[3930.0,10.])
f['hydrostatic_boundary_LP'].attrs.create('Origin',[0.,-5.])

f['hydrostatic_boundary_GP'].attrs.create('Dimension',['XZ'],dtype='|S2')
f['hydrostatic_boundary_GP'].attrs.create('Discretization',[3930.0,10.])
f['hydrostatic_boundary_GP'].attrs.create('Origin',[0.,-5.])

f['hydrostatic_boundary_GS'].attrs.create('Dimension',['XZ'],dtype='|S2')
f['hydrostatic_boundary_GS'].attrs.create('Discretization',[3930.0,10.])
f['hydrostatic_boundary_GS'].attrs.create('Origin',[0.,-5.])

#close 'em down
f.close()
f2.close()
f3.close()
