#before running on skybridge (or redsky) do
#module load canopy/1.4.1

import sys
import os
try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import pflotran as pft

mpl.rcParams['font.size']=18
#mpl.rcParams['font.weight']='bold'
mpl.rcParams['lines.linewidth']=3

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=18, fontweight='bold')
    plt.ylabel('Liquid Saturation (0)', fontsize=18, fontweight='bold')

    #plt.xlim(-1000.,1.e6)
    #plt.ylim(24., 31.)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.semilogx(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  string = data.get_name('yname').split()[3].split('_')[1]
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=string)
    #  plt.loglog(data.get_array('x'),data.get_array('y'),label=data.get_name('yname'))
    #  plt.axis([0.1, 1.e6, 200., 0.]) #why is this here?
    #or label with file name
    #  plt.plot(data.get_array('x'),data.get_array('y'),label=filenames[ifile])
    #or label with name assigned above
      string = name[icol]
      plt.semilogx(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],label=string)

path = []
path.append('.')

titles = []
files = []
columns = []
name = []
linestyles = []
colors = []
basestyles = ['-' for x in range(2)]
basecolors = ['red','darkorange',] #'yellowgreen','green']

#12PWR
titles.append('Nearfield Liquid Saturation')
files.append('../scenarios/uz12pwr_oscillating_state/pflotran-obs-0.tec')
columns.append(4)
name.append('wp2 12pwr')
files.append('../scenarios/uz12pwr_oscillating_state/pflotran-obs-0.tec')
columns.append(25)
name.append('above wp2 12pwr')

linestyles.extend(basestyles)
colors.extend(basecolors)

#37PWR
files.append('../scenarios/uz37pwr_dryout/pflotran-obs-0.tec')
columns.append(4)
name.append('wp2 37pwr')
files.append('../scenarios/uz37pwr_dryout/pflotran-obs-0.tec')
columns.append(25)
name.append('above wp2 37pwr')

linestyles.extend(['--' for x in range(2)])
colors.extend(basecolors)

filenames = pft.get_full_paths(path,files)

f = plt.figure(figsize=(8,8)) #adjust size to fit plots (width, height)
#f.suptitle("Cement, line loads: Temperature (degrees C)",fontsize=16)

make_subplots(1,1,1,filenames,columns,name,titles)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
#plt.legend(loc=(1.20,0.175),title='FRACTURE REALIZATION')
plt.legend(loc=0)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize=18,fontweight='normal')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('lsaturation.png',bbox_inches='tight')
plt.show()
