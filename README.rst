*********************
GDSA Multiphase Tests
*********************

This repository stores computationally challenging multiphase flow and transportsimulations that are based on potential GDSA (Geologic Disposal Safety Assessment) reference cases.  

Please create a separate directory for each scenario with the appropriate reference case directroy.
For instance, a scenario demonstrating dryout in the unsaturated zone could be placed in a directory named 'dry_out_in_uz' and located under the uz reference case directory as shown below.
Please place scripts (python, sh, etc.) for scenarios within the highest level 'scripts' directory possible to minimize code/script replication.  We want to maximize reuse and minimize the number of scripts lying around. For instance, the write_xmf.py script can be used by all scenarios utilizing explicit unstructured grids and has been placed in the top-level 'scripts' directory.


Directory Structure
===================

::

  gdsa_multiphase_tests
  |
  |--scripts
  |
  |--shale
  |  |
  |  |--scenarios
  |  |  |
  |  |  |--<a scenario>
  |  |  |
  |  |  |--<another scenario>
  |  |  |
  |  |  |...
  |  |
  |  |--scripts
  |
  |--uz
  |  |
  |  |--scenarios
  |  |  |
  |  |  |--dry_out_in_uz
  |  |
  |  |--scripts
  |
  |--<another reference case>
  
