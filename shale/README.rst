*****************
Shale Scenarios
*****************

Scenario 1
================
./scenarios/clay12pwr_2d_unstruct uses a 2-D unstructured grid with single 12-pwr wp. A single state oscillation occurs in a single cell as the drift is resaturating. 12-PWR heat is not hot enough to boil at 5 MPa ambient pressure.

A structured 2D grid with 5x5 m^2 grid cells did not have any state oscillations.


Scenario 2
===============
./scenarios/clay12pwr_3d_unstruct uses a 3-D unstructured grid with a single 12-pwr wp. State oscillations occur in several cells as the drift is resaturating. (and as above, 12-PWR heat is not hot enought to boil at 5 MPa ambient pressure.)
