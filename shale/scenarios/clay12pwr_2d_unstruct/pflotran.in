# System: Shale (aka clay) with single 12-PWR heat source
#
# 2D implicit unstructured grid with 15, 5 and 5/3 m grid cell lengths
# Materials: shale, drz, buffer, wp
#
# Size: 45 m x 5 m x 165 m (231 hexes) (really 2D, not fake 2D)
#
# hydrostatic initial conditions! (so handy) with unsaturated (2-phase) drift
# no-flow lateral boundaries (reflection)
# dirichlet top and bottom boundaries
# ~5 MPa and ~25 C ambient conditions
#
# Issue: oscillating state changes. State oscillates as cells of drift resaturate,
# so oscillation is understandable, but perhaps solve could be more efficient.
# Single oscillation at cell 191 - if you need more see clay12pwr_3d_unstruct.
# A 2D structured grid had no oscillations.

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE GENERAL 
      OPTIONS
        USE_INFINITY_NORM_CONVERGENCE
        ANALYTICAL_DERIVATIVES
        FIX_UPWIND_DIRECTION
      /
    /
  /
END

SUBSURFACE

#=========================== chemistry ========================================
#=========================== discretization ===================================
GRID
  TYPE unstructured ../2d_grid/clay_2d_usg.h5
END

#=========================== solver options ===================================
skip
TIMESTEPPER FLOW
  TS_ACCELERATION 10 #LENGTH OF DT_FACTOR ARRAY
  #num newt 1    2     3      4     5      6    7     8     9     10
  DT_FACTOR 2.d0 1.5d0 1.25d0 1.1d0 1.05d0 1.d0 0.8d0 0.6d0 0.4d0 0.33d0
END

NEWTON_SOLVER FLOW
  ATOL 1.d-12
  RTOL 1.d-8
  STOL 1.d-12
  MAXIT 20
END

LINEAR_SOLVER FLOW
END
noskip
#=========================== times ============================================
TIME
  FINAL_TIME 1.d06 y
  INITIAL_TIMESTEP_SIZE 1.d-6 y
  MAXIMUM_TIMESTEP_SIZE 1. y at 1. y
  MAXIMUM_TIMESTEP_SIZE 5. y at 10. y
  MAXIMUM_TIMESTEP_SIZE 50. y at 100. y
  MAXIMUM_TIMESTEP_SIZE 500. y at 1000. y
  MAXIMUM_TIMESTEP_SIZE 5000. y at 10000. y
END

#=========================== output options ===================================
OUTPUT
  SNAPSHOT_FILE
    FORMAT HDF5
    PERIODIC TIME 0.1 y between 0. y and 1. y
    PERIODIC TIME 1. y between 0. y and 10. y
    PERIODIC TIME 10. y between 0. y and 100. y
    PERIODIC TIME 100. y between 0. y and 1000. y
    PERIODIC TIME 1000. y between 0. y and 10000. y
    PERIODIC TIME 10000. y between 0. y and 100000. y
    PERIODIC TIME 100000. y between 0. y and 1000000. y
  /
  OBSERVATION_FILE
    PERIODIC TIMESTEP 1
  /
  MASS_BALANCE_FILE
    PERIODIC TIMESTEP 1
  /
  VARIABLES
    TEMPERATURE
    LIQUID_PRESSURE
    LIQUID_SATURATION
    LIQUID_DENSITY
    LIQUID_MOBILITY
    LIQUID_MOLE_FRACTIONS
    GAS_PRESSURE
    GAS_SATURATION
    GAS_DENSITY
    GAS_MOBILITY
    GAS_MOLE_FRACTIONS
    CAPILLARY_PRESSURE
    THERMODYNAMIC_STATE
    MATERIAL_ID
    NATURAL_ID
  /
  VELOCITY_AT_CENTER
END

#=========================== observation points ===============================
OBSERVATION
  REGION wp_obs
END

OBSERVATION
  REGION buffer_obs
END

OBSERVATION
  REGION drz_obs
END

OBSERVATION
  REGION shale_obs
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  PHASE LIQUID
  DIFFUSION_COEFFICIENT 1.d-9
END

FLUID_PROPERTY
  PHASE GAS
  DIFFUSION_COEFFICIENT 2.1d-5
END

#=========================== material properties ==============================
#natural barrier
MATERIAL_PROPERTY shale
  ID 1
  CHARACTERISTIC_CURVES shale
  POROSITY 0.20
  #TORTUOSITY 0.11 #Van Loon and Mibus 2015, logDe = 2.4logphi(eff)+logDw (I think) 
  TORTUOSITY_FUNCTION_OF_POROSITY 1.4 #Van Loon and Mibus 2015
  SOIL_COMPRESSIBILITY 1.6d-8 #1/Pa, Konikow and Neuzil 2007
  SOIL_COMPRESSIBILITY_FUNCTION LEIJNSE
  SOIL_REFERENCE_PRESSURE 101325.d0
  ROCK_DENSITY 2700.
  THERMAL_CONDUCTIVITY_DRY 0.6d0 #educated guess
  THERMAL_CONDUCTIVITY_WET 1.2d0
  HEAT_CAPACITY 830.
  PERMEABILITY
    PERM_ISO 1.d-19
  /
END

MATERIAL_PROPERTY drz
  ID 2
  CHARACTERISTIC_CURVES shale
  POROSITY 0.20
  #TORTUOSITY 0.11 #Van Loon and Mibus 2015, logDe = 2.4logphi(eff)+logDw (I think) 
  TORTUOSITY_FUNCTION_OF_POROSITY 1.4 #Van Loon and Mibus 2015
  SOIL_COMPRESSIBILITY 1.6d-8 #1/Pa, Konikow and Neuzil 2007
  SOIL_COMPRESSIBILITY_FUNCTION LEIJNSE
  SOIL_REFERENCE_PRESSURE 101325.d0
  ROCK_DENSITY 2700.
  THERMAL_CONDUCTIVITY_DRY 0.6d0 #educated guess
  THERMAL_CONDUCTIVITY_WET 1.2d0
  HEAT_CAPACITY 830.
  PERMEABILITY
    PERM_ISO 1.d-18
  /
END

#engineered barrier
MATERIAL_PROPERTY buffer
  ID 3
  CHARACTERISTIC_CURVES bentonite
  POROSITY 0.35 #porosity of .3SiO2+.7MX80 dry compacted Liu et al. 2016
  #TORTUOSITY 0.23 #Van Loon and Mibus 2015, logDe = 2.4logphi(eff)+logDw (I think)
  TORTUOSITY_FUNCTION_OF_POROSITY 1.4 #Van Loon and Mibus 2015
  SOIL_COMPRESSIBILITY 1.6d-8 #1/Pa
  SOIL_COMPRESSIBILITY_FUNCTION LEIJNSE
  SOIL_REFERENCE_PRESSURE 101325.d0
  ROCK_DENSITY 2700.
  THERMAL_CONDUCTIVITY_DRY 0.6d0 #difficult to read from Wangetal2015
  THERMAL_CONDUCTIVITY_WET 1.5d0 #Jobman&Buntebarth2009;Wangetal2015;Tangetal2008;Zhengetal2015
  HEAT_CAPACITY 830.
  PERMEABILITY
    PERM_ISO  1.d-20 #5.d-21 would be for pure bentonite & results in unstable runs. e.g. Zihms&Harrington2015
  /                  #1.d-20 is for bentonite/sand mixture, e.g. Liu et al 2016
/

MATERIAL_PROPERTY wp #stainless steel thermal properties as in previous GDSA (Mariner et al 2015,2016)
  ID 4
  CHARACTERISTIC_CURVES default
  POROSITY 0.50 #0.30
  TORTUOSITY 1.0 #allow things to diffuse out quickly
  SOIL_COMPRESSIBILITY 0.d0
  SOIL_COMPRESSIBILITY_FUNCTION LEIJNSE
  SOIL_REFERENCE_PRESSURE 101325.d0
  ROCK_DENSITY 5000.d0
  THERMAL_CONDUCTIVITY_DRY 16.7d0
  THERMAL_CONDUCTIVITY_WET 16.7d0
  HEAT_CAPACITY 466.
  PERMEABILITY
    PERM_ISO 1.d-16
  /
/

#=========================== characteristic curves ============================
CHARACTERISTIC_CURVES default
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA 1.d-4
    M 0.5
    LIQUID_RESIDUAL_SATURATION 0.1d0
    MAX_CAPILLARY_PRESSURE 1.d7
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    PHASE LIQUID
    M 0.5
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    PHASE GAS
    M 0.5
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
  /
END

CHARACTERISTIC_CURVES bentonite
  SATURATION_FUNCTION VAN_GENUCHTEN
    #crazy high air entry pressure. what does that mean?
    #clay has chemico-physico suction in addition to capillarity
    #hence very high air entry pressure - e.g. Jacinto et al. 2009
    ALPHA 6.25d-8 #Justinavicius et al. 2016
    M 0.375 #Justinavicius et al. 2016
    LIQUID_RESIDUAL_SATURATION 0.1d0
    MAX_CAPILLARY_PRESSURE 1.d8 #roughly read from Jacinto et al. 2009
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    PHASE LIQUID
    M 0.375
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    PHASE GAS
    M 0.375
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
  /
END

CHARACTERISTIC_CURVES shale #and drz
  SATURATION_FUNCTION VAN_GENUCHTEN
    #crazy high air entry pressure. what does that mean?
    #clay has chemico-physico suction in addition to capillarity
    #hence very high air entry pressure - e.g. Jacinto et al. 2009
    ALPHA 6.67d-7 #Justinavicius et al. 2016
    M 0.333 #Justinavicius et al. 2016
    LIQUID_RESIDUAL_SATURATION 0.1d0
    MAX_CAPILLARY_PRESSURE 1.d8 #roughly read from Jacinto et al. 2009
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    PHASE LIQUID
    M 0.333
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    PHASE GAS
    M 0.333
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
  /
END

#=========================== regions ==========================================
REGION all
  COORDINATES
    0.d0 0.d0 0.d0
    45.d0 5.d0 165.d0
  /
END

REGION top
  FACE top
  COORDINATES
    0.d0 0.d0 165.d0
    45.d0 5.d0 165.d0
  /
END

REGION bottom
  FACE bottom
  COORDINATES
    0.d0 0.d0 0.d0
    45.d0 5.d0 0.d0
  /
END

REGION drz
  COORDINATES
    18.333333d0 0.d0 78.3333333d0
    26.666666d0 5.d0 86.666666d0
  /
END

REGION buffer
  COORDINATES
    20.d0 0.d0 80.d0
    25.d0 5.d0 85.d0
  /
END 
    
REGION wp
  COORDINATES
    21.666666d0 0.d0 81.666666d0
    23.333333d0 5.d0 83.333333d0
  /
END

#=========================== observation regions ===============================
REGION wp_obs
  COORDINATE 22.5d0 2.5d0 82.5d0
END

REGION buffer_obs
  COORDINATE 22.5d0 2.5d0 84.166666d0
END

REGION drz_obs
  COORDINATE 22.5d0 2.5d0 85.833333d0
END

REGION shale_obs
  COORDINATE 22.5d0 2.5d0 87.5d0
END

#=========================== flow conditions ==================================
FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE HYDROSTATIC #hydrostatic ic is not going to work in general mode?
    MOLE_FRACTION DIRICHLET
    TEMPERATURE DIRICHLET
  /
  DATUM 0.d0 0.d0 75.d0
  GRADIENT
    TEMPERATURE 0.d0 0.d0 -0.025d0 #C/m
  /
  LIQUID_PRESSURE 5.d6 #this repository is ~500 m below surface
  MOLE_FRACTION 1.d-8
  TEMPERATURE 25.d0
END

FLOW_CONDITION unsaturated
  TYPE
    GAS_PRESSURE DIRICHLET
    GAS_SATURATION DIRICHLET
    TEMPERATURE DIRICHLET
  /
  GAS_PRESSURE 101325.d0
  GAS_SATURATION 0.7d0
  TEMPERATURE 25.d0
END

FLOW_CONDITION wp_heatsource
  TYPE
    RATE SCALED_MASS_RATE VOLUME #volume average
  /
  #SYNC_TIMESTEP_WITH_UPDATE
  INTERPOLATION LINEAR
  RATE FILE ../../../common/12pwr_general.txt
END

#=========================== transport conditions =============================
#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  FLOW_CONDITION initial
  REGION all
END

INITIAL_CONDITION
  FLOW_CONDITION unsaturated
  REGION buffer
END

#boundary condition
BOUNDARY_CONDITION
  FLOW_CONDITION initial
  REGION top
END

BOUNDARY_CONDITION
  FLOW_CONDITION initial
  REGION bottom
END

# source_sink
SOURCE_SINK
  FLOW_CONDITION wp_heatsource
  REGION wp
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL shale
END

STRATA
  REGION drz
  MATERIAL drz
END

STRATA
  REGION buffer
  MATERIAL buffer
END

STRATA
  REGION wp
  MATERIAL wp
END

#=========================== end subsurface ===================================
END_SUBSURFACE

