#list_obs.py
#create list of column headers and numbers for all observation points
#to use: edit path to directory containing obs files (line 10)
#        create list of obs files by copying and editing line 15.
#Emily 12/20/18

import os

#run this from outside the simulation directory
#and only use one path at a time, or disaster will occur
paths = []
paths.append('../scenarios/clay12pwr_2d_unstruct')
#or else use this:
#paths.append('.')

names = []
names.append('pflotran-obs-0.tec')

filenames = []
for ipath in range(len(paths)):
  for iname in range(len(names)):
    filename = []
    filename.append(paths[ipath])
    filename.append(names[iname])
    filename = '/'.join(filename)
    filenames.append(filename)

fileout_name = os.path.join(paths[0],'obs_list.txt')
fileout = open(fileout_name,'w')
#fileout = open(paths[0]+'_obs_list.txt', 'w')
files = []
# open files and determine column ids
for ifilename in range(len(filenames)):
  fileout.write(filenames[ifilename]+'\n')
  files.append(open(filenames[ifilename],'r'))
  header = files[ifilename].readline()
  headings = header.split(',')
  for icol in range(len(headings)):
    fileout.write('%d: %s\n' % (icol+1, headings[icol]))
fileout.close()
for ifile in range(len(files)):
  files[ifile].close()
print 'done'

